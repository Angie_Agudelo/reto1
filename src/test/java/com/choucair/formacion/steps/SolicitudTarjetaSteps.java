package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.SolicitudTarjetasPage;

import net.thucydides.core.annotations.Step;

public class SolicitudTarjetaSteps {

	SolicitudTarjetasPage solicitudTarjetasPage;
	
	@Step
	public void ingresar_home() {
		solicitudTarjetasPage.open();
		solicitudTarjetasPage.VerificaHome();		
	}
	
	public void ingresar_productos_servicios(){
		solicitudTarjetasPage.VerificaProductosServicios();
	}
	
	public void ingresar_tarjeta(){
		solicitudTarjetasPage.VerificaTarjeta();
	}
	
	public void informacion_tarjeta(){
		solicitudTarjetasPage.InformacionAmericanExpress();
		solicitudTarjetasPage.InformacionMasterCard();
	}
	
	public void ingresar_formulario_solicitud(){
		solicitudTarjetasPage.IngresarOpcionSolicitalaAqui();
	}
	
	public void diligenciar_tabla_paso_uno(List<List<String>> data, int i) {
		solicitudTarjetasPage.IngresarFrame();
		solicitudTarjetasPage.Nombres(data.get(i).get(0).trim());
		solicitudTarjetasPage.Apellidos(data.get(i).get(1).trim());
		solicitudTarjetasPage.TipoDoc(data.get(i).get(2).trim());
		solicitudTarjetasPage.NumeroDoc(data.get(i).get(3).trim());
		solicitudTarjetasPage.FechaNac(data.get(i).get(4).trim());
		solicitudTarjetasPage.Ingresos(data.get(i).get(5).trim());
		solicitudTarjetasPage.Ciudad(data.get(i).get(6).trim());
		solicitudTarjetasPage.Continuar();
	}
	
	public void verificar_confirmacion() {
	 solicitudTarjetasPage.Confirmacion();
	}
}
