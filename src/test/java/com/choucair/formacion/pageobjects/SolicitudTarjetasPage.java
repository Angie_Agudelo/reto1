package com.choucair.formacion.pageobjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;


import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.grupobancolombia.com/wps/portal/personas")
public class SolicitudTarjetasPage extends PageObject{
	
	//Opción productos y servicios
	@FindBy(xpath = "//*[@id=\'main-menu\']/div[2]/ul[1]/li[3]/a")
	public WebElementFacade Productos;
	
	//Label productos y servicios
	@FindBy(xpath = "//*[@id=\'productosPersonas\']/div/div[1]/div/h2")
	public WebElementFacade lblProductos;
	
	//Label Home
	@FindBy(xpath = "//*[@id=\'main-content\']/div[1]/div/div[4]/div/div/div/a")
	public WebElementFacade lblHome;
	
	//Opción Tarjeta de crédito
	@FindBy(xpath = "//*[@id=\'productosPersonas\']/div/div[1]/div/div/div[7]/div/a")
	public WebElementFacade Tarjeta;
	
	//Label Tarjeta
	@FindBy(xpath = "//*[@id=\'main-content\']/div[1]/div[1]/div/div[2]/div/div[1]/h2")
	public WebElementFacade lblTarjeta;
	
	//Titulo American Express
	@FindBy(xpath = "//*[@id=\'card_0\']/div[2]/h2")
	public WebElementFacade lblTituloAmerican;
	
	//Texto American Express
	@FindBy(xpath = "//*[@id=\'card_0\']/div[3]/ul")
	public WebElementFacade lblTextoAmerican;
	
	//Titulo MasterCard
	@FindBy(xpath = "//*[@id=\'card_1\']/div[2]/h2")
	public WebElementFacade lblTituloMaster;
	
	//Texto MasterCard
	@FindBy(xpath = "//*[@id=\'card_1\']/div[3]/ul")
	public WebElementFacade lblTextoMaster;	
	
	//Ingresar a la opción solicitala aquí
	@FindBy(xpath = "//*[@id=\'card_0\']/div[4]/a")
	public WebElementFacade btnSolicitala;	
	
	//Label Solicitud
	@FindBy(xpath = "//*[@id=\'div_descripcion\']/div/div/div/h1")
	public WebElementFacade lblSolicitud;	
	
	//Campo Nombres
	@FindBy(id="nombresReq")
	public WebElementFacade txtNombres;
	
	//Campo Apellidos
	@FindBy(id="apellidosReq")
	public WebElementFacade txtApellidos;
	
	//Campo Tipo de documento
	@FindBy(id="typedocreq")
	public WebElementFacade cmbTipoDoc;
	
	//Campo Número de documento
	@FindBy(id="numeroDocumento")
	public WebElementFacade txtNumeroDoc;
	
	//Campo fecha de nacimiento
	@FindBy(id="fechaNacimientoReq")
	public WebElementFacade txtFechaNac;
	
	//Campo ingresos mensuales
	@FindBy(id="ingresos-mensuales")
	public WebElementFacade txtIngresos;
	
	//Campo Ciudad y departamento
	@FindBy(id="reqCiuidadDpto_value")
	public WebElementFacade txtCiudadDepartamento;
	
	//Botón continuar paso uno
	@FindBy(xpath = "//*[@id=\'conIframe_rm\']/div/form/div[7]/div[2]/button")
	public WebElementFacade btnContinuarp1;
	
	//Label confirmación
	@FindBy(xpath = "//*[@id=\'conIframe_ip\']/div/form/div[1]/div/h2")
	public WebElementFacade lblDatosPersonales;
	
	//Formulario 
	@FindBy(xpath = "//*[@id=\'reqCiuidadDpto_dropdown\']/div[3]/div")
	public WebElementFacade FSolicitud;
	
	
	public void VerificaHome() {
		String labelH = "Descubre más";
		String strMensaje = lblHome.getText();
		assertThat(strMensaje, containsString(labelH));
	}
	
	
	public void VerificaProductosServicios() {
		Productos.click();
		String labelP = "Productos y Servicios";
		String strMensaje = lblProductos.getText();
		assertThat(strMensaje, containsString(labelP));
	}
	
	
	public void VerificaTarjeta() {
		Tarjeta.click();
		String labelT = "Escoge la tarjeta perfecta para cada momento";
		String strMensaje = lblTarjeta.getText();
		assertThat(strMensaje, containsString(labelT));
	}
	
	public void InformacionAmericanExpress() {
		String strTitulo = lblTituloAmerican.getText();
		String strTexto = lblTextoAmerican.getText();
		System.out.println(strTitulo+"\n"+strTexto);
	}
	
	public void InformacionMasterCard() {
		String strTitulo = lblTituloMaster.getText();
		String strTexto = lblTextoMaster.getText();
		System.out.println(strTitulo+"\n"+strTexto);
	}
	
	public void IngresarOpcionSolicitalaAqui() {
		btnSolicitala.click();
		String labelS = "Solicitud Tarjeta de Crédito";
		String strMensaje = lblSolicitud.getText();
		assertThat(strMensaje, containsString(labelS));
	}
	public void IngresarFrame() {
		getDriver().switchTo().frame("Demos");
	}
	
	public void Nombres(String Dato) {
		txtNombres.click();
		txtNombres.clear();
		txtNombres.sendKeys(Dato);
	}
	public void Apellidos(String Dato) {
		txtApellidos.click();
		txtApellidos.clear();
		txtApellidos.sendKeys(Dato);
	}
	public void TipoDoc(String Dato) {
		cmbTipoDoc.click();
		cmbTipoDoc.selectByVisibleText(Dato);
	}
	public void NumeroDoc(String Dato) {
		txtNumeroDoc.click();
		txtNumeroDoc.clear();
		txtNumeroDoc.sendKeys(Dato);
	}
	public void FechaNac(String Dato) {
		txtFechaNac.click();
		txtFechaNac.clear();
		txtFechaNac.sendKeys(Dato);
	}
	public void Ingresos(String Dato) {
		txtIngresos.click();
		txtIngresos.clear();
		txtIngresos.sendKeys(Dato);
	}
	public void Ciudad(String Dato) {
		txtCiudadDepartamento.click();
		txtCiudadDepartamento.clear();
		txtCiudadDepartamento.sendKeys(Dato);
		FSolicitud.click();
	}
	public void Continuar() {
		btnContinuarp1.click();
	}
	
	public void Confirmacion() {
		String labelDP = "Datos personales";
		String strMensaje = lblDatosPersonales.getText();
		assertThat(strMensaje, containsString(labelDP));
	}
	
		
}
