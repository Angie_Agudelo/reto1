package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.SolicitudTarjetaSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class SolicitudTarjetaDefinition {

	@Steps
	SolicitudTarjetaSteps solicitudTarjetaSteps;
	
	@Given("^Ingresar al inicio$")
	public void ingresar_al_inicio() {
		solicitudTarjetaSteps.ingresar_home();
	}
	
	@Given("^Ingresar a la opción Productos y Servicios$")
	public void ingresar_a_la_opción_Productos_y_Servicios() throws Throwable {
		solicitudTarjetaSteps.ingresar_productos_servicios();
	}

	@Given("^Ingresar a la opción  Tarjetas de Crédito$")
	public void ingresar_a_la_opción_Tarjetas_de_Crédito() throws Throwable {
		solicitudTarjetaSteps.ingresar_tarjeta();
	}

	@Given("^Mostrar información tarjeta de crédito American Express y Mastercard Black$")
	public void mostrar_información_tarjeta_de_crédito_American_Express_y_Mastercard_Black() throws Throwable {
		solicitudTarjetaSteps.informacion_tarjeta();
	}

	@Given("^Ingresar a la opción Solicitala aquí de la tarjeta American Express$")
	public void ingresar_a_la_opción_Solicitala_aquí_de_la_tarjeta_American_Express() throws Throwable {
		solicitudTarjetaSteps.ingresar_formulario_solicitud();
	}

	@When("^Diligenciar el formulario de Solicitud de tarjeta de crédito pasouno$")
	public void diligenciar_el_formulario_de_Solicitud_de_tarjeta_de_crédito_pasouno(DataTable dtDatosPasoUno) throws Throwable {
		List<List<String>> data = dtDatosPasoUno.raw();
		
		for (int i=1; i<data.size(); i++){
			solicitudTarjetaSteps.diligenciar_tabla_paso_uno(data, i);
		}
	}

	@Then("^Verificar envío exitoso$")
	public void verificar_envío_exitoso() throws Throwable {
		solicitudTarjetaSteps.verificar_confirmacion();
	}

}
