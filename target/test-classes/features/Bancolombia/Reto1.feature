#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Regresion
Feature: Reto1
  El usuario debe poder ingresar a la opción Productos y servicios. 
  Posteriormente poder ingresar a la opción tarjetas de crédito, 
  donde puede ver la información detalladas de cada una de las tarjetas
  Y finalmente debe poder diligenciar el formulario de solicitud de la tarjeta American Express.

  @CasoExitoso
  Scenario: Solicitud tarjeta de crédito American Express
    Given Ingresar al inicio
    And Ingresar a la opción Productos y Servicios 
    And Ingresar a la opción  Tarjetas de Crédito
    And Mostrar información tarjeta de crédito American Express y Mastercard Black
    And Ingresar a la opción Solicitala aquí de la tarjeta American Express
    When Diligenciar el formulario de Solicitud de tarjeta de crédito pasouno
   			 |Nombres| Apellidos| Tipo documento			| Numero de documento| Fecha nacimiento| Ingresos| Ciudad/ Departamento|
   			 |Juan	 | Perez		|Cédula de Ciudadanía	| 		 1234567890		 | 	 1990-01-01		 | 2500000 | Medellin - Antioquia| 
    Then Verificar envío exitoso

#  @tag2
#  Scenario Outline: Title of your scenario outline
#    Given I want to write a step with <name>
#    When I check for the <value> in step
#    Then I verify the <status> in step

#    Examples: 
#      | name  | value | status  |
#      | name1 |     5 | success |
#      | name2 |     7 | Fail    |
